# Define environment-specific variables
DEV_ENV = development
STAG_ENV = staging
PROD_ENV = production
TEST_ENV = testing

.PHONY: dev stag prod test

# Targets for each environment
dev:
	@echo "Deploying to $(DEV_ENV) environment"
	# Add deployment commands for Dev environment here

stag:
	@echo "Deploying to $(STAG_ENV) environment"
	# Add deployment commands for Staging environment here

prod:
	@echo "Deploying to $(PROD_ENV) environment"
	# Add deployment commands for Production environment here

test:
	@echo "Deploying to $(TEST_ENV) environment"
	# Add deployment commands for Testing environment here

# Target for running deployment based on the environment variable passed
deploy:
	@if [ -z "$(ENV)" ]; then \
		echo "Please provide an environment (e.g., 'make deploy ENV=dev')"; \
		exit 1; \
	fi
	@$(ENV)
